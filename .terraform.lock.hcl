# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/gitlabhq/gitlab" {
  version     = "16.7.0"
  constraints = "16.7.0"
  hashes = [
    "h1:yi/QQ2JA3R2DQwyC89tFsqaCbu1uk8EapK8y7K5uxlY=",
    "zh:213e3f0e6f00a1cc69445a7250158ca06ec8ebe5a6840468aa23923238976025",
    "zh:24f05d4c32b4359144d1b91fdc2bfbed35476f93cf597de10acc1d8caeb835cc",
    "zh:2b063dce902cef1a809f33ab78de5a61a957f76728f020fa24640f0ef317bacf",
    "zh:36a958b8f5705b304dd89a4cc6826840328c57a75dc837b9a1492639c2d79a82",
    "zh:446c640a49218e9785231cb5d41af305044d6f0e4f5c7f25d5b898d5c0b6fcb6",
    "zh:57ea65f73957e0b23e8108fbdd1354f787e751510949c98ddf2991d5d1d427d9",
    "zh:7d6c71d12cf2e2b3af53e87f6f902558f00fc6a7fdfceacc5ef043f343ccd2aa",
    "zh:8206c001d1a5ed86a0fcd40e6d65509f565082431549be2da317b355cc911318",
    "zh:942db736188af9c2fed0c22b874ce6346e50acd998ca8b714118bfe570665480",
    "zh:adbb26cac349561568d479186ce0edf18e01fb4e3e3095defc64ab5e0ff13408",
    "zh:c13bbcd0f927bdf01e76fbb704ee3b06008acec540ce9f4629ff23834c0339aa",
    "zh:c5ec2b75da3264c86df860da2638bac1032289ceda3c3b9a463638987b2151d4",
    "zh:e1c76cb5c440113da842e9f7d41076bef90afc0cd778961499307e1858e759f9",
    "zh:f456aaddefcf58b5b730626a1b81f96cffaa89d0a58249a8a8d19ddf850fb6ce",
    "zh:f809ab383cca0a5f83072981c64208cbd7fa67e986a86ee02dd2c82333221e32",
  ]
}
