# My GitLab Configuration

## Getting started / disaster recovery

1. Create a personal access token with full access.
2. Create a new project in GitLab.
3. Set `TF_VAR_gitlab_token` as a masked variable in the new project.
4. Push the main branch to the project that you made in step 2.
5. Run the pipeline fully.
