# To avoid chicken-and-egg problems, I will not import this project in Terraform.

resource "gitlab_project" "gitlab-ci-templates" {
  name             = "GitLab CI Templates"
  description      = "My collection of GitLab CI templates"
  visibility_level = "public"

  tags = [
    "GitLab",
    "gitlab-ci",
    "GitLab CI",
    "gitlab-ci-templates",
    "gitlab-ci templates",
  ]
}

resource "gitlab_project" "gitlab-ci-templates-python-example" {
  name             = "Gitlab CI Templates Python Example"
  description      = "Demo for my GitLab CI template for Python"
  visibility_level = "public"

  tags = [
    "GitLab",
    "gitlab-ci",
    "GitLab CI",
    "gitlab-ci-templates",
    "gitlab-ci templates",
    "python"
  ]
}

resource "gitlab_project" "gitlab-ci-templates-docker-example" {
  name             = "Gitlab CI Templates Docker Example"
  description      = "Demo for my GitLab CI template for Docker"
  visibility_level = "public"

  tags = [
    "GitLab",
    "gitlab-ci",
    "GitLab CI",
    "gitlab-ci-templates",
    "gitlab-ci templates",
    "docker"
  ]
}

resource "gitlab_project" "gitlab-ci-utils" {
  name             = "GitLab CI Utilities"
  path             = "gitlab-ci-utils"
  description      = "🏗️ In progress"
  visibility_level = "public"
}

resource "gitlab_project" "terraform-aws-iam-permissions" {
  name             = "AWS IAM Permissions for Terraform"
  path             = "terraform-aws-iam-permissions"
  description      = "Permissions for Terraform to manage my AWS account"
  visibility_level = "public"

  tags = [
    "iac",
    "iac:terraform",
    "#IaC",
    "Infrastructure-as-Code (IaC)",
    "infrastructure-as-code",
    "infrastructure as code",
    "InfrastructureAsCode",
    "terraform",
    "managed-by-terraform",
    "managed by terraform",
    "managed_by=terraform",
    "ManagedBy=Terraform",
    "aws",
    "aws-iam",
  ]
}

resource "gitlab_project" "terraform-aws-free-tier" {
  name             = "My Infrastructure in AWS Free Tier"
  path             = "terraform-aws-free-tier"
  description      = "My AWS account configuration; permissions are separately managed here: https://gitlab.com/ginolatorilla/terraform-aws-iam-permissions/"
  visibility_level = "public"

  tags = [
    "iac",
    "iac:terraform",
    "#IaC",
    "Infrastructure-as-Code (IaC)",
    "infrastructure-as-code",
    "infrastructure as code",
    "InfrastructureAsCode",
    "terraform",
    "managed-by-terraform",
    "managed by terraform",
    "managed_by=terraform",
    "ManagedBy=Terraform",
    "aws",
  ]
}

resource "gitlab_project" "terraform-cloudflare-config" {
  name             = "My CloudFlare Configuration"
  path             = "terraform-cloudflare-config"
  description      = "My CloudFlare account configuration"
  visibility_level = "public"

  tags = [
    "iac",
    "iac:terraform",
    "#IaC",
    "Infrastructure-as-Code (IaC)",
    "infrastructure-as-code",
    "infrastructure as code",
    "InfrastructureAsCode",
    "terraform",
    "managed-by-terraform",
    "managed by terraform",
    "managed_by=terraform",
    "ManagedBy=Terraform",
    "cloudflare"
  ]
}

resource "gitlab_project" "terraform-github-config" {
  name             = "My GitHub Configuration"
  path             = "terraform-github-config"
  description      = "My GitHub account configuration (https://github.com/ginolatorilla)"
  visibility_level = "public"

  tags = [
    "iac",
    "iac:terraform",
    "#IaC",
    "Infrastructure-as-Code (IaC)",
    "infrastructure-as-code",
    "infrastructure as code",
    "InfrastructureAsCode",
    "terraform",
    "managed-by-terraform",
    "managed by terraform",
    "managed_by=terraform",
    "ManagedBy=Terraform",
    "github",
  ]
}

resource "gitlab_project" "gitlab-ci-recipes" {
  name             = "GitLab CI Recipes"
  path             = "gitlab-ci-recipes"
  description      = "My CI snippets"
  visibility_level = "public"

  tags = [
    "GitLab",
    "gitlab-ci",
    "GitLab CI",
    "gitlab-ci-templates",
    "gitlab-ci templates",
  ]
}
