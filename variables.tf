variable "gitlab_token" {
  description = "A GitLab access token."
  sensitive   = true
  validation {
    condition     = length(var.gitlab_token) > 0
    error_message = "gitlab_token cannot be an empty string."
  }
}
